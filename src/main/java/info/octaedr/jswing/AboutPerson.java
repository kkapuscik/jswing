/*------------------------------------------------------------------------------
- This file is part of OCTjswing project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jswing;

/**
 * Information about single person in about data.
 *
 * @author Krzysztof Kapuscik
 */
public class AboutPerson {

    /** Name of the person. */
    private final String name;
    /** Task of the person. */
    private final String task;
    /** Email address. */
    private final String emailAddress;
    /** Web (home page) address. */
    private final String webAddress;

    /**
     * Constructs new person information.
     *
     * @param name
     *            Name of the person.
     */
    public AboutPerson(final String name) {
        this(name, null, null, null);
    }

    /**
     * Constructs new person information.
     *
     * @param name
     *            Name of the person.
     * @param task
     *            Task of the person.
     */
    public AboutPerson(final String name, final String task) {
        this(name, task, null, null);
    }

    /**
     * Constructs new person information.
     *
     * @param name
     *            Name of the person.
     * @param task
     *            Task of the person.
     * @param emailAddress
     *            Contact email address.
     */
    public AboutPerson(final String name, final String task, final String emailAddress) {
        this(name, task, emailAddress, null);
    }

    /**
     * Constructs new person information.
     *
     * @param name
     *            Name of the person.
     * @param task
     *            Task of the person.
     * @param emailAddress
     *            Contact email address.
     * @param webAddress
     *            Web (home page) address.
     */
    public AboutPerson(final String name, final String task, final String emailAddress, final String webAddress) {
        if (name == null) {
            throw new IllegalArgumentException("Name may not be null");
        }

        this.name = name;
        this.task = task;
        this.emailAddress = emailAddress;
        this.webAddress = webAddress;
    }

    /**
     * Returns name of the person.
     *
     * @return Name of the person or null if not set.
     */
    public String getName() {
        return this.name;
    }

    /**
     * Returns task of the person.
     *
     * @return Task of the person or null if not set.
     */
    public String getTask() {
        return this.task;
    }

    /**
     * Returns email address.
     *
     * @return Email address or null if not set.
     */
    public String getEmailAddress() {
        return this.emailAddress;
    }

    /**
     * Returns web address.
     *
     * @return Web addresss or null if not set.
     */
    public String getWebAddress() {
        return this.webAddress;
    }

}
