/*------------------------------------------------------------------------------
- This file is part of OCTjswing project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jswing;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.Icon;
import javax.swing.KeyStroke;

/**
 * Extended version of Swing's AbstractAction.
 *
 * @author Krzysztof Kapuscik
 */
@SuppressWarnings("serial")
public class SwingAction extends AbstractAction {

    /**
     * The key used for storing the user object for the action.
     */
    public static final String USER_OBJECT_KEY = "OCT.UserObject";

    /**
     * The key used to mark action as a toggle action.
     */
    public static final String TOGGLE_ACTION = "OCT.ToggleAction";

    /** Executor to be called when action is performed. */
    private SwingActionExecutor executor;

    /**
     * Constructs action without any elements set.
     */
    public SwingAction() {
        // nothing to do
    }

    /**
     * Returns name.
     *
     * @return Name or null if not set.
     *
     * @see Action#NAME
     */
    public String getName() {
        return (String) getValue(NAME);
    }

    /**
     * Sets name.
     *
     * @param name
     *            Value to set or null to clear.
     *
     * @see Action#NAME
     */
    public void setName(final String name) {
        putValue(NAME, name);
    }

    /**
     * Returns short description.
     *
     * @return Short description or null if not set.
     *
     * @see Action#SHORT_DESCRIPTION
     */
    public String getShortDescription() {
        return (String) getValue(SHORT_DESCRIPTION);
    }

    /**
     * Sets short description.
     *
     * @param shortDescription
     *            Value to set or null to clear.
     *
     * @see Action#SHORT_DESCRIPTION
     */
    public void setShortDescription(final String shortDescription) {
        putValue(SHORT_DESCRIPTION, shortDescription);
    }

    /**
     * Returns long description.
     *
     * @return Long description or null if not set.
     *
     * @see Action#LONG_DESCRIPTION
     */
    public String getLongDescription() {
        return (String) getValue(LONG_DESCRIPTION);
    }

    /**
     * Sets long description.
     *
     * @param longDescription
     *            Value to set or null to clear.
     *
     * @see Action#LONG_DESCRIPTION
     */
    public void setLongDescription(final String longDescription) {
        putValue(LONG_DESCRIPTION, longDescription);
    }

    /**
     * Returns small icon.
     *
     * @return Small icon or null if not set.
     *
     * @see Action#SMALL_ICON
     */
    public Icon getSmallIcon() {
        return (Icon) getValue(SMALL_ICON);
    }

    /**
     * Sets small icon.
     *
     * @param smallIcon
     *            Value to set or null to clear.
     *
     * @see Action#SMALL_ICON
     */
    public void setSmallIcon(final Icon smallIcon) {
        putValue(SMALL_ICON, smallIcon);
    }

    /**
     * Returns action command.
     *
     * @return Action command or null if not set.
     *
     * @see Action#ACTION_COMMAND_KEY
     */
    public String getActionCommand() {
        return (String) getValue(ACTION_COMMAND_KEY);
    }

    /**
     * Sets action command.
     *
     * @param actionCommand
     *            Value to set or null to clear.
     *
     * @see Action#ACTION_COMMAND_KEY
     */
    public void setActionCommand(final String actionCommand) {
        putValue(ACTION_COMMAND_KEY, actionCommand);
    }

    /**
     * Returns accelerator.
     *
     * @return Accelerator or null if not set.
     *
     * @see Action#ACCELERATOR_KEY
     */
    public KeyStroke getAccelerator() {
        return (KeyStroke) getValue(ACCELERATOR_KEY);
    }

    /**
     * Sets accelerator.
     *
     * @param accelerator
     *            Value to set or null to clear.
     *
     * @see Action#ACCELERATOR_KEY
     */
    public void setAccelerator(final KeyStroke accelerator) {
        putValue(ACCELERATOR_KEY, accelerator);
    }

    /**
     * Returns mnemonic key.
     *
     * @return Mnemonic key or null if not set.
     *
     * @see Action#MNEMONIC_KEY
     */
    public Integer getMnemonicKey() {
        return (Integer) getValue(MNEMONIC_KEY);
    }

    /**
     * Sets mnemonic key.
     *
     * @param mnemonic
     *            Value to set or null to clear.
     *
     * @see Action#MNEMONIC_KEY
     */
    public void setMnemonicKey(final Integer mnemonic) {
        putValue(MNEMONIC_KEY, mnemonic);
    }

    /**
     * Checks selection status.
     *
     * @return True if selection is set to true, false if it is set to false or not set at all.
     *
     * @see Action#SELECTED_KEY
     */
    public boolean isSelected() {
        return Boolean.TRUE.equals(getSelected());
    }

    /**
     * Returns selection status.
     *
     * @return Selection status or null if not set.
     *
     * @see Action#SELECTED_KEY
     */
    public Boolean getSelected() {
        return (Boolean) getValue(SELECTED_KEY);
    }

    /**
     * Sets selection status.
     *
     * @param selected
     *            Value to set or null to clear.
     *
     * @see Action#SELECTED_KEY
     */
    public void setSelected(final Boolean selected) {
        putValue(SELECTED_KEY, selected);
    }

    /**
     * Returns displayed mnemonic index.
     *
     * @return Index or null if not set.
     *
     * @see Action#DISPLAYED_MNEMONIC_INDEX_KEY
     */
    public Integer getDisplayedMnemonicIndex() {
        return (Integer) getValue(DISPLAYED_MNEMONIC_INDEX_KEY);
    }

    /**
     * Sets displayed mnemonic index.
     *
     * @param index
     *            Value to set or null to clear.
     *
     * @see Action#DISPLAYED_MNEMONIC_INDEX_KEY
     */
    public void setDisplayedMnemonicIndex(final Integer index) {
        putValue(DISPLAYED_MNEMONIC_INDEX_KEY, index);
    }

    /**
     * Returns large icon.
     *
     * @return Large icon or null if not set.
     *
     * @see Action#LARGE_ICON_KEY
     */
    public Icon getLargeIcon() {
        return (Icon) getValue(LARGE_ICON_KEY);
    }

    /**
     * Sets large icon.
     *
     * @param largeIcon
     *            Value to set or null to clear.
     *
     * @see Action#LARGE_ICON_KEY
     */
    public void setLargeIcon(final Icon largeIcon) {
        putValue(LARGE_ICON_KEY, largeIcon);
    }

    /**
     * Returns user object.
     *
     * @return User object or null if not set.
     *
     * @see #USER_OBJECT_KEY
     */
    public Object getUserObject() {
        return getValue(USER_OBJECT_KEY);
    }

    /**
     * Sets user object.
     *
     * @param userObject
     *            Value to set or null to clear.
     *
     * @see #USER_OBJECT_KEY
     */
    public void setUserObject(final Object userObject) {
        putValue(USER_OBJECT_KEY, userObject);
    }

    /**
     * Sets executor to be called when action is performed.
     *
     * @param executor
     *            Executor instance to set or null to unset the currently set executor.
     */
    public void setActionExecutor(SwingActionExecutor executor) {
        this.executor = executor;
    }

    /**
     * Calls the executor if set.
     */
    @Override
    public void actionPerformed(ActionEvent event) {
        if (this.executor != null) {
            this.executor.actionPerformed(event);
        }
    }

}
