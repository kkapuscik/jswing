/*------------------------------------------------------------------------------
- This file is part of OCTjswing project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jswing;

/** Column descriptor for table model. */
public class ColumnDescriptor {

    /** Name of the column. */
    private final String name;
    /** Class of the items in column. */
    private final Class<?> itemClass;

    /**
     * Constructs new column descriptor.
     *
     * @param name
     *            Name of the column.
     * @param itemClass
     *            Class of the items in column.
     */
    public ColumnDescriptor(final String name, Class<?> itemClass) {
        this.name = name;
        this.itemClass = itemClass;
    }

    /**
     * Returns column name.
     *
     * @return Column name.
     */
    public String getName() {
        return this.name;
    }

    /**
     * Returns class of items in column.
     *
     * @return Class of item in column.
     */
    public Class<?> getItemClass() {
        return this.itemClass;
    }

}
