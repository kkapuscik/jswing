/*------------------------------------------------------------------------------
- This file is part of OCTjswing project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jswing;

import java.awt.event.ActionEvent;

/**
 * Executor that could be registered in SwingAction to perform operations when action is performed.
 *
 * @author Krzysztof Kapuscik
 */
public interface SwingActionExecutor {

    /**
     * Invoked when an action occurs.
     *
     * @param event
     *            Event descriptor.
     */
    void actionPerformed(ActionEvent event);

}
