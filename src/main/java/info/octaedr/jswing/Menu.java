/*------------------------------------------------------------------------------
- This file is part of OCTjswing project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jswing;

import javax.swing.Action;
import javax.swing.JButton;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JMenu;
import javax.swing.JMenuItem;

/**
 * Menu is a JMenu component with some extension added.
 *
 * <p>
 * The list of extension:
 * <ul>
 * <li>Automatic JToggleButton creation on addAction() if action is selectable.
 * </ul>
 * </p>
 *
 * @author Krzysztof Kapuscik
 */
@SuppressWarnings("serial")
public class Menu extends JMenu {

    /**
     * Constructs a new <code>Menu</code> with no text.
     */
    public Menu() {
        super();
    }

    /**
     * Constructs a new <code>Menu</code> with the supplied string as its text.
     *
     * @param s
     *            The text for the menu label.
     */
    public Menu(String s) {
        super(s);
    }

    /**
     * Creates a new menu item attached to the specified <code>Action</code> object and appends it to the end of this
     * menu.
     *
     * @param action
     *            The <code>Action</code> for the menu item to be added.
     *
     * @return The new component which dispatches the action
     */
    public JMenuItem add(SwingAction action) {
        if (action == null) {
            throw new NullPointerException("Null action given");
        }

        JMenuItem menuItem;

        if (action.getValue(Action.SELECTED_KEY) != null) {
            menuItem = new JCheckBoxMenuItem(action);
        } else {
            menuItem = new JMenuItem(action);
        }

        menuItem.setHorizontalTextPosition(JButton.TRAILING);
        menuItem.setVerticalTextPosition(JButton.CENTER);

        add(menuItem);

        return menuItem;
    }

}
