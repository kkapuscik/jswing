/*------------------------------------------------------------------------------
- This file is part of OCTjswing project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jswing;

import javax.swing.AbstractButton;
import javax.swing.Action;
import javax.swing.JButton;
import javax.swing.JToggleButton;
import javax.swing.JToolBar;

/**
 * ToolBar is a JToolbar component with some extension added.
 *
 * <p>
 * The list of extension:
 * <ul>
 * <li>Automatic JToggleButton creation on addAction() if action is selectable.
 * </ul>
 * </p>
 *
 * @author Krzysztof Kapuscik
 */
@SuppressWarnings("serial")
public class ToolBar extends JToolBar {

    /**
     * Creates a new tool bar.
     *
     * <p>
     * Orientation defaults to <code>HORIZONTAL</code>.
     * </p>
     */
    public ToolBar() {
        super();
    }

    /**
     * Creates a new tool bar with the specified <code>orientation</code>.
     *
     * <p>
     * The <code>orientation</code> must be either <code>HORIZONTAL</code> or <code>VERTICAL</code>.
     * </p>
     *
     * @param orientation
     *            The orientation desired.
     */
    public ToolBar(int orientation) {
        super(orientation);
    }

    /**
     * Creates a new tool bar with the specified <code>name</code>.
     *
     * <p>
     * The name is used as the title of the undocked tool bar. The default orientation is <code>HORIZONTAL</code>.
     * </p>
     *
     * @param name
     *            The name of the tool bar.
     */
    public ToolBar(String name) {
        super(name);
    }

    /**
     * Creates a new tool bar with a specified <code>name</code> and <code>orientation</code>.
     *
     * <p>
     * The <code>orientation</code> must be either <code>HORIZONTAL</code> or <code>VERTICAL</code>.
     * </p>
     *
     * @param name
     *            The name of the tool bar.
     * @param orientation
     *            The initial orientation.
     *
     * @exception IllegalArgumentException
     *                if orientation is neither <code>HORIZONTAL</code> nor <code>VERTICAL</code>
     */
    public ToolBar(String name, int orientation) {
        super(name, orientation);
    }

    /**
     * Adds a new component which dispatches the action.
     *
     * @param action
     *            A the <code>Action</code> object to add as a new item.
     *
     * @return The new component which dispatches the action
     */
    public AbstractButton add(SwingAction action) {
        if (action == null) {
            throw new NullPointerException("Null action given");
        }

        AbstractButton button;

        if (action.getValue(Action.SELECTED_KEY) != null) {
            button = new JToggleButton(action);
        } else {
            button = new JButton(action);
        }

        if (action.getSmallIcon() != null || action.getLargeIcon() != null) {
            button.setHideActionText(true);
        }

        button.setHorizontalTextPosition(JButton.CENTER);
        button.setVerticalTextPosition(JButton.BOTTOM);

        add(button);

        return button;
    }

}
