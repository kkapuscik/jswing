/*------------------------------------------------------------------------------
- This file is part of OCTjswing project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jswing;

import javax.swing.JLabel;

import org.jdesktop.swingx.JXFrame;
import org.jdesktop.swingx.JXStatusBar;

/**
 * Base class for application main frames.
 *
 * @author Krzysztof Kapuscik
 */
@SuppressWarnings("serial")
public class MainFrame extends JXFrame {

    /** Default frame width. */
    public static final int DEFAULT_WIDTH = 1000;
    /** Default frame height. */
    public static final int DEFAULT_HEIGTH = 700;

    /** Information about application. */
    private final AboutData aboutData;
    /** Status bar message component. */
    private JLabel statusText;

    /**
     * Constructs new frame.
     *
     * @param aboutData
     *            Information about application.
     */
    public MainFrame(final AboutData aboutData) {
        this(DEFAULT_WIDTH, DEFAULT_HEIGTH, aboutData);

        setLocationRelativeTo(null);
    }

    /**
     * Constructs new frame.
     *
     * @param width
     *            Width of the frame.
     * @param heigth
     *            Height of the frame.
     * @param aboutData
     *            Information about application.
     */
    public MainFrame(final int width, final int heigth, final AboutData aboutData) {
        super(aboutData.getProgramName(), true);

        this.aboutData = aboutData;

        setSize(width, heigth);
        setLocationRelativeTo(null);

        setStatusBar(createStatusBar());
    }

    /**
     * Constructs status bar.
     *
     * @return Created status bar component.
     */
    protected JXStatusBar createStatusBar() {
        /* create the status text component */
        this.statusText = new JLabel("Ready!");

        /* build the status bar */
        JXStatusBar statusBar = new JXStatusBar();
        JXStatusBar.Constraint constraint = new JXStatusBar.Constraint(JXStatusBar.Constraint.ResizeBehavior.FILL);
        statusBar.add(this.statusText, constraint);

        return statusBar;
    }

    /**
     * Sets message to be displayed on status bar.
     *
     * @param message
     *            Message to be displayed.
     */
    public void setStatusText(final String message) {
        this.statusText.setText(message);
    }

    /**
     * Shows application about dialog.
     *
     * <p>
     * The displayed dialog is <strong>modal</strong>.
     * </p>
     */
    protected void showAboutDialog() {
        AboutDialog aboutDialog = new AboutDialog(this.aboutData, this, true);
        aboutDialog.setVisible(true);
    }

}
