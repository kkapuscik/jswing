/*------------------------------------------------------------------------------
- This file is part of OCTjswing project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jswing.resources;

import java.awt.Image;
import java.awt.Toolkit;

import javax.swing.Icon;
import javax.swing.ImageIcon;

/**
 * Base class for specialized resource providers.
 *
 * <p>
 * The final resource name is constructed as concatenation of set package name, the version key, the resource key, and
 * extension.<br>
 * For example for given package name <code>"com.example.myapp.icons"</code>, version key <code>"small"</code>, resource
 * key <code>"new-document"</code> and extension <code>"png"</code> the produced resource name is
 * <code>"com/example/myapp/icons/small/new-document.png"</code>.
 * </p>
 *
 * @author Krzysztof Kapuscik
 */
public class ResourceProvider {

    /** Package name. */
    private final String pkgName;
    /** Package path. */
    private final String pkgPath;

    /**
     * Constructs resource provider.
     *
     * @param pkgName
     *            Name of the package (java package, with dot separated segments).
     */
    public ResourceProvider(final String pkgName) {
        this.pkgName = pkgName;
        this.pkgPath = "/" + this.pkgName.replace('.', '/') + "/";
    }

    /**
     * Returns requested icon.
     *
     * @param resourceKey
     *            Key of the resource.
     * @param versionKey
     *            Version of the resource.
     * @param extension
     *            Resource extension
     *
     * @return Icon object or null if not available.
     */
    public Icon getIcon(final String resourceKey, final String versionKey, final String extension) {
        try {
            String iconPath = this.pkgPath + versionKey + '/' + resourceKey + '.' + extension;
            return new ImageIcon(getClass().getResource(iconPath));
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * Returns requested image.
     *
     * @param resourceKey
     *            Key of the resource.
     * @param versionKey
     *            Version of the resource.
     * @param extension
     *            Resource extension
     *
     * @return Image object or null if not available.
     */
    public Image getImage(final String resourceKey, final String versionKey, final String extension) {
        try {
            String iconPath = this.pkgPath + versionKey + '/' + resourceKey + '.' + extension;
            return Toolkit.getDefaultToolkit().getImage(getClass().getResource(iconPath));
        } catch (Exception e) {
            return null;
        }
    }

}
