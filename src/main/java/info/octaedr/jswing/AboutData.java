/*------------------------------------------------------------------------------
- This file is part of OCTjswing project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jswing;

import java.awt.image.BufferedImage;
import java.util.ArrayList;

/**
 * Information about application.
 */
public class AboutData {

    /** License type - Custom. */
    public static final int LICENSE_TYPE__CUSTOM = 0;

    /** License type - GPL. */
    public static final int LICENSE_TYPE__GPL = 1;

    /** License type - GPL version 2. */
    public static final int LICENSE_TYPE__GPLv2 = 2;

    /** License type - GPL version 3. */
    public static final int LICENSE_TYPE__GPLv3 = 3;

    /** License type - Lesser/Library GPL. */
    public static final int LICENSE_TYPE__LGPL = 4;

    /** License type - Lesser/Library GPL version 2. */
    public static final int LICENSE_TYPE__LGPLv2 = 5;

    /** License type - BSD. */
    public static final int LICENSE_TYPE__BSD = 6;

    /** List of application authors. */
    private ArrayList<AboutPerson> authors = new ArrayList<AboutPerson>();

    /** List of persons who contributed. */
    private ArrayList<AboutPerson> credits = new ArrayList<AboutPerson>();

    /** List of translators. */
    private ArrayList<AboutPerson> translators = new ArrayList<AboutPerson>();

    /**
     * A displayable program name string.
     *
     * Example: "KEdit".
     */
    private String programName;

    /**
     * The program version string.
     *
     * Example: "2.0.1".
     */
    private String version;

    /**
     * A short description of what the program does.
     *
     * Example: "A simple text editor.".
     */
    private String shortDescription;

    /**
     * License text.
     */
    private String licenseText;

    /**
     * A copyright statement.
     *
     * Example: "(c) 1999-2000, Name".
     */
    private String copyrightStatement;

    /**
     * Some free form text, that can contain any kind of information.
     *
     * The text can contain newlines.
     */
    private String otherText;

    /**
     * The program homepage string.
     *
     * Start the address with "http://".
     *
     * Example: "http://some.domain.org/".
     */
    private String homePageAddress;

    /**
     * The bug report email address string.
     *
     * Example: "bugz@mynewprogram.org".
     */
    private String bugEmailAddress;

    /** Application's icon. */
    private BufferedImage appIcon;

    /**
     * Constructs basic AboutData instance.
     *
     * @param programName
     *            A displayable program name string.
     * @param version
     *            The program version string.
     */
    public AboutData(final String programName, final String version) {
        setProgramName(programName);
        setVersion(version);
    }

    /**
     * Construct AboutData instance.
     *
     * @param programName
     *            A displayable program name string.
     * @param version
     *            The program version string.
     * @param shortDescription
     *            A short description of what the program does.
     * @param licenseType
     *            Type of the license.
     * @param copyrightStatement
     *            A copyright statement.
     * @param otherText
     *            Some free form text.
     * @param homePageAddress
     *            The program homepage string.
     * @param bugEmailAddress
     *            The bug report email address string.
     */
    public AboutData(final String programName, final String version, final String shortDescription,
            final int licenseType, final String copyrightStatement, final String otherText,
            final String homePageAddress, final String bugEmailAddress) {
        setProgramName(programName);
        setVersion(version);
        setShortDescription(shortDescription);
        setLicenseType(licenseType);
        setCopyrightStatement(copyrightStatement);
        setOtherTest(otherText);
        setHomePageAddress(homePageAddress);
        setBugEmailAddress(bugEmailAddress);
    }

    /**
     * Defines an author.
     *
     * @param author
     *            Author to be appended to list.
     */
    public void addAuthor(final AboutPerson author) {
        this.authors.add(author);
    }

    /**
     * Defines a person that deserves credit.
     *
     * @param person
     *            Person to be appended to list.
     */
    public void addCredit(final AboutPerson person) {
        this.credits.add(person);
    }

    /**
     * Defines a tranlator.
     *
     * @param translator
     *            Translator to be appended to list.
     */
    public void addTranslator(final AboutPerson translator) {
        this.translators.add(translator);
    }

    /**
     * Sets license type.
     *
     * This method may be used to set one of predefined license types. For custom licenses the
     * {@link #setCustomLicense(String)} should be used.
     *
     * @param type
     *            Type of the license.
     */
    public void setLicenseType(final int type) {
        this.licenseText = getLicenseText(type);
    }

    /**
     * Sets custom license text.
     *
     * @param licenseText
     *            Custom license text to set.
     */
    public void setCustomLicense(final String licenseText) {
        this.licenseText = licenseText;
    }

    /**
     * Sets program name.
     *
     * @param programName
     *            Program name to set.
     */
    public void setProgramName(final String programName) {
        this.programName = programName;
    }

    /**
     * Sets application version string.
     *
     * @param version
     *            Version string to set.
     */
    public void setVersion(final String version) {
        this.version = version;
    }

    /**
     * Sets short description.
     *
     * @param description
     *            Short description to set.
     */
    public void setShortDescription(final String description) {
        this.shortDescription = description;
    }

    /**
     * Sets copyright statement.
     *
     * @param copyrightStatement
     *            Copyright statement to set.
     */
    public void setCopyrightStatement(final String copyrightStatement) {
        this.copyrightStatement = copyrightStatement;
    }

    /**
     * Sets free form text.
     *
     * @param text
     *            Text to set.
     */
    public void setOtherTest(final String text) {
        this.otherText = text;
    }

    /**
     * Sets application homepage address.
     *
     * @param homePageAddress
     *            Application homepage address to set.
     */
    public void setHomePageAddress(final String homePageAddress) {
        this.homePageAddress = homePageAddress;
    }

    /**
     * Sets bug reporting email address.
     *
     * @param bugEmailAddress
     *            Bug reporting email address to set.
     */
    public void setBugEmailAddress(final String bugEmailAddress) {
        this.bugEmailAddress = bugEmailAddress;
    }

    /**
     * Sets application icon.
     *
     * @param icon
     *            Icon to set.
     */
    public void setIcon(final BufferedImage icon) {
        this.appIcon = icon;
    }

    /**
     * Returns application authors.
     *
     * @return Collection of authors.
     */
    public AboutPerson[] getAuthors() {
        return this.authors.toArray(new AboutPerson[this.authors.size()]);
    }

    /**
     * Returns credited persons.
     *
     * @return Collection of credited persons.
     */
    public AboutPerson[] getCredits() {
        return this.credits.toArray(new AboutPerson[this.credits.size()]);
    }

    /**
     * Returns application translators.
     *
     * @return Collection of translators.
     */
    public AboutPerson[] getTranslators() {
        return this.translators.toArray(new AboutPerson[this.translators.size()]);
    }

    /**
     * Returns program name.
     *
     * @return Program name.
     */
    public String getProgramName() {
        return this.programName;
    }

    /**
     * Returns version string.
     *
     * @return Version string of application.
     */
    public String getVersion() {
        return this.version;
    }

    /**
     * Returns short description.
     *
     * @return Application short description.
     */
    public String getShortDescription() {
        return this.shortDescription;
    }

    /**
     * Returns license text.
     *
     * @return License text.
     */
    public String getLicenseText() {
        return this.licenseText;
    }

    /**
     * Returns copyright statement.
     *
     * @return Copyright statement.
     */
    public String getCopyrightStatement() {
        return this.copyrightStatement;
    }

    /**
     * Returns free form text.
     *
     * @return Free form text.
     */
    public String getOtherText() {
        return this.otherText;
    }

    /**
     * Returns homepage address.
     *
     * @return Application homepage address.
     */
    public String getHomePageAddress() {
        return this.homePageAddress;
    }

    /**
     * Returns bug reporting email address.
     *
     * @return Bug reporting email address.
     */
    public String getBugEmailAddress() {
        return this.bugEmailAddress;
    }

    /**
     * Returns application icon.
     *
     * @return Application icon.
     */
    public BufferedImage getIcon() {
        return this.appIcon;
    }

    /**
     * Returns license text if available.
     *
     * @param licenseType
     *            Type of the license.
     *
     * @return License text if available.
     */
    private static String getLicenseText(int licenseType) {
        switch (licenseType) {
            case LICENSE_TYPE__GPL:
            case LICENSE_TYPE__GPLv2:
            case LICENSE_TYPE__GPLv3:
            case LICENSE_TYPE__LGPL:
            case LICENSE_TYPE__LGPLv2:
            case LICENSE_TYPE__BSD:
            default:
                // TODO
                break;
        }
        return null;
    }

}
