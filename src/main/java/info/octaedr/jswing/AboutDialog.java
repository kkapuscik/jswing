/*------------------------------------------------------------------------------
- This file is part of OCTjswing project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jswing;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.Frame;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;

import org.jdesktop.swingx.JXPanel;
import org.jdesktop.swingx.painter.ImagePainter;

/**
 * Generic dialog for showing information about application.
 *
 * @author Krzysztof Kapuscik
 */
@SuppressWarnings("serial")
public class AboutDialog extends JDialog {

    /** Default width of the dialog. */
    private static final int DEFAULT_WIDTH = 400;

    /** Default height of the dialog. */
    private static final int DEFAULT_HEIGHT = 300;

    /**
     * Constructs new dialog.
     *
     * @param aboutData
     *            Data to be presented.
     * @param owner
     *            Owner of the dialog.
     * @param modal
     *            Modal dialog flag.
     */
    public AboutDialog(final AboutData aboutData, final Frame owner, final boolean modal) {
        super(owner, "About " + aboutData.getProgramName(), modal);

        /* build components & layout */
        createComponents(aboutData);

        /* set size & position */
        setSize(DEFAULT_WIDTH, DEFAULT_HEIGHT);
        setLocationRelativeTo(null);
    }

    /**
     * Creates dialog components.
     *
     * @param data
     *            Data to be presented.
     */
    private void createComponents(final AboutData data) {

        Component topComponent = createTopComponent(data);
        Component bottomComponent = createBottomComponent();
        JTabbedPane tabsPane = new JTabbedPane();

        Component component;

        component = createProgramInfoComponent(data);
        if (component != null) {
            tabsPane.addTab("Application", component);
        }

        component = createAuthorsComponent(data);
        if (component != null) {
            tabsPane.addTab("Authors", component);
        }

        component = createCreditsComponent(data);
        if (component != null) {
            tabsPane.addTab("Credits", component);
        }

        component = createTranslatorsComponent(data);
        if (component != null) {
            tabsPane.addTab("Translators", component);
        }

        component = createLicenseComponent(data);
        if (component != null) {
            tabsPane.addTab("License", component);
        }

        JPanel mainPanel = new JPanel(new BorderLayout(5, 5));
        mainPanel.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
        mainPanel.add(topComponent, BorderLayout.NORTH);
        mainPanel.add(tabsPane, BorderLayout.CENTER);
        mainPanel.add(bottomComponent, BorderLayout.SOUTH);

        setContentPane(mainPanel);
    }

    /**
     * Creates single text area component.
     *
     * @param text
     *            Text to be presented.
     *
     * @return Created component.
     */
    private Component createTextAreaComponent(final String text) {
        JTextArea licenceTextArea = new JTextArea(text);
        licenceTextArea.setEditable(false);
        licenceTextArea.setMargin(new Insets(5, 5, 5, 5));

        return new JScrollPane(licenceTextArea);
    }

    /**
     * Creates license component.
     *
     * @param data
     *            Data to be presented.
     *
     * @return Created component.
     */
    private Component createLicenseComponent(final AboutData data) {
        String licenseText = data.getLicenseText();
        if (licenseText == null) {
            return null;
        }

        return createTextAreaComponent(licenseText);
    }

    /**
     * Creates component with translators data.
     *
     * @param data
     *            Data to be presented.
     *
     * @return Created component.
     */
    private Component createTranslatorsComponent(final AboutData data) {
        if (data.getTranslators().length == 0) {
            return null;
        }

        StringBuilder builder = new StringBuilder();

        preparePersonsText(builder, data.getTranslators());

        return createTextAreaComponent(builder.toString());
    }

    /**
     * Creates component with credits data.
     *
     * @param data
     *            Data to be presented.
     *
     * @return Created component.
     */
    private Component createCreditsComponent(final AboutData data) {
        if (data.getCredits().length == 0) {
            return null;
        }

        StringBuilder builder = new StringBuilder();

        preparePersonsText(builder, data.getCredits());

        return createTextAreaComponent(builder.toString());
    }

    /**
     * Creates component with authors data.
     *
     * @param data
     *            Data to be presented.
     *
     * @return Created component.
     */
    private Component createAuthorsComponent(final AboutData data) {
        if (data.getAuthors().length == 0 && data.getBugEmailAddress() == null) {
            return null;
        }

        StringBuilder builder = new StringBuilder();

        if (data.getBugEmailAddress() != null) {
            builder.append("Bugs can be reported to:\n\t");
            builder.append(data.getBugEmailAddress());
            builder.append('\n');
        }

        preparePersonsText(builder, data.getAuthors());

        return createTextAreaComponent(builder.toString());
    }

    /**
     * Appends information about a group of people to given builder.
     *
     * @param builder
     *            Builder to append data to.
     * @param persons
     *            Group of people.
     */
    private void preparePersonsText(final StringBuilder builder, final AboutPerson[] persons) {
        for (int i = 0; i < persons.length; ++i) {
            if (builder.length() > 0) {
                builder.append("\n");
            }

            if (persons[i].getName() != null) {
                builder.append(persons[i].getName());
                builder.append('\n');
            }

            if (persons[i].getTask() != null) {
                builder.append('\t');
                builder.append(persons[i].getTask());
                builder.append('\n');
            }

            if (persons[i].getEmailAddress() != null) {
                builder.append('\t');
                builder.append(persons[i].getEmailAddress());
                builder.append('\n');
            }

            if (persons[i].getWebAddress() != null) {
                builder.append('\t');
                builder.append(persons[i].getWebAddress());
                builder.append('\n');
            }
        }
    }

    /**
     * Creates component with program info.
     *
     * @param data
     *            Data to be presented.
     *
     * @return Created component.
     */
    private Component createProgramInfoComponent(final AboutData data) {
        StringBuilder builder = new StringBuilder();

        if (data.getProgramName() != null) {
            builder.append(data.getProgramName());
            builder.append('\n');
        }

        if (data.getShortDescription() != null) {
            builder.append(data.getShortDescription());
            builder.append('\n');
        }

        if (data.getCopyrightStatement() != null) {
            if (builder.length() > 0) {
                builder.append('\n');
            }
            builder.append("Copyright (C) ");
            builder.append(data.getCopyrightStatement());
            builder.append('\n');
            builder.append("All Rights Reserved\n");
        }

        if (data.getOtherText() != null) {
            if (builder.length() > 0) {
                builder.append('\n');
            }
            builder.append(data.getOtherText());
            builder.append('\n');
        }

        if (data.getHomePageAddress() != null) {
            if (builder.length() > 0) {
                builder.append('\n');
            }
            builder.append(data.getHomePageAddress());
            builder.append('\n');
        }

        return createTextAreaComponent(builder.toString());
    }

    /**
     * Creates components at bottom of dialog (close button).
     *
     * @return Created component.
     */
    private Component createBottomComponent() {
        JButton closeButton = new JButton("Close");
        closeButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                AboutDialog.this.dispose();
            }
        });

        Container bottomContainer = new Container();
        bottomContainer.setLayout(new BorderLayout(5, 5));
        bottomContainer.add(closeButton, BorderLayout.EAST);

        return bottomContainer;
    }

    /**
     * Creates component at top of dialog (program icon, name & version).
     *
     * @param data
     *            Data to be presented.
     *
     * @return Created component.
     */
    private Component createTopComponent(final AboutData data) {
        ImagePainter iconPainter = null;
        JXPanel iconPanel = null;
        JLabel appNameLabel = null;

        if (data.getIcon() != null) {
            iconPainter = new ImagePainter(data.getIcon());

            iconPanel = new JXPanel();
            iconPanel.setBackgroundPainter(iconPainter);
        }

        String appName = data.getProgramName();
        if (appName == null) {
            appName = "-unknown name-";
        }
        if (data.getVersion() != null) {
            appName += " " + data.getVersion();
        }
        appNameLabel = new JLabel(appName);

        Container topContainer = new Container();
        topContainer.setLayout(new BorderLayout(5, 5));
        if (iconPanel != null) {
            topContainer.add(iconPanel, BorderLayout.WEST);
        }
        topContainer.add(appNameLabel, BorderLayout.CENTER);

        return topContainer;
    }

}
