/*------------------------------------------------------------------------------
- This file is part of OCTjswing project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jswing;

import javax.swing.table.AbstractTableModel;

/**
 * Implementation of TableModel that extends AbstractTableModel and adds support for static column definitions. To be
 * used as a base class of TableModel implementations.
 *
 * @author Krzysztof Kapuscik
 */
@SuppressWarnings("serial")
public abstract class TableModelBase extends AbstractTableModel {

    /** Empty string constant. */
    protected static final String EMPTY_STRING = "";

    /**
     * Definitions of columns.
     */
    private final ColumnDescriptor[] columns;

    /**
     * Constructs new model.
     *
     * @param columns
     *            Definitions of columns.
     */
    protected TableModelBase(ColumnDescriptor[] columns) {
        if (columns == null) {
            throw new NullPointerException("Null columns");
        }

        this.columns = columns;
    }

    @Override
    public int getColumnCount() {
        return this.columns.length;
    }

    @Override
    public String getColumnName(int columnIndex) {
        if (columnIndex >= 0 && columnIndex < this.columns.length) {
            return this.columns[columnIndex].getName();
        } else {
            return EMPTY_STRING;
        }
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        if (columnIndex >= 0 && columnIndex < this.columns.length) {
            return this.columns[columnIndex].getItemClass();
        } else {
            return Object.class;
        }
    }

}
